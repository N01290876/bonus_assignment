﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cart.aspx.cs" Inherits="BonusAssignment1.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


        <label>X Value:</label>
        <asp:TextBox runat="server" ID="xvalue"></asp:TextBox>
    <asp:RequiredFieldValidator ID="xaxisrequired" controlTovalidate="xvalue"  runat="server" ErrorMessage="required" ></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ControlToValidate="xvalue" runat="server" ID="chechk" 
        ValidationExpression="^[-+]?\d*[1-9]\d*\.?[0]*$"  ErrorMessage="not a valid Format"></asp:RegularExpressionValidator>
    <br />
        <label>Y Value:</label> 
        <asp:TextBox runat="server" ID="yvalue"></asp:TextBox>
     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" controlTovalidate="yvalue"  runat="server" ErrorMessage="required" ></asp:RequiredFieldValidator>
    <asp:RegularExpressionValidator ControlToValidate="yvalue" runat="server" ID="RegularExpressionValidator1" 
        ValidationExpression="^[-+]?\d*[1-9]\d*\.?[0]*$"  ErrorMessage="not a valid Format"></asp:RegularExpressionValidator>
    
        <asp:Button runat="server" ID="myButton" OnClick="Button_Click" Text="Submit Coordinates"/>
    <br />

    <div id="result" runat="server"></div>
       
</asp:Content>
