﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_Click(object sender, EventArgs e)
        {
            int x = int.Parse(xvalue.Text);
            int y = int.Parse(yvalue.Text);

            if (x > 0 && y > 0)
            {
                result.InnerHtml = "The coordinates (" + x+","+y+") is in first quadrant";


            }
            if (x > 0 && y < 0)
            {
                result.InnerHtml = "The coordinates (" + x + "," + y + ") is in fourth quadrant";


            }


            if (x < 0 && y > 0)
            {
                result.InnerHtml = "The coordinates (" + x + "," + y + ") is in second quadrant";


            }
            if (x < 0 && y < 0)
            {
                result.InnerHtml = "The coordinates (" + x + "," + y + ") is in third quadrant";


            }
        }

    }
}